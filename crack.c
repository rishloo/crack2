#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    
    // Hash the guess using MD5
     guess = md5(guess, HASH_LEN);
     // Compare the two hashes
    if (!(strcmp(hash, guess)))
        return 0;
    else
        return 1;
    // Free any malloc'd memory
    for ( int i = 0; i < ( strlen(guess) / strlen(guess[0]) ); i++ )
    {
        free( guess[i] );
    }
    free(guess);
    


}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{
    //*size = 0;
    int lines = 50;
    //allocate memory for string array
    char ** strarr = malloc(lines*sizeof(char *));
    //read in the dictionary file for reading
    FILE *f = fopen(filename, "r");
    char line[1000];
    int count = 0;
    //read from the dictionary file, one word per line
    while ( fgets(line, 1000, f) != NULL )
    {
        if (count==lines)
        {
            //double the amt of lines
            lines *= 2;
            //reallocate memory to accomodate for incr size
            strarr = realloc(strarr, lines*sizeof(char *));
        }
        
        //null terminate the end of each line
        line[strlen(line)-1] = '\0';
        //allocate memory for each word 
        char *word = malloc(strlen(line) *sizeof(char) + 1);
        //copy line to word
        strcpy(word, line);
        //assign word to element of string array
        strarr[count] = word;
        count++;
    }
    fclose(f);
    //store the length of the array in size
    //amt of elements, not amt of bytes
    size = sizeof(strarr) / sizeof(char);
    //return string array
    return strarr;
    
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of strings.
    int dlen = sizeof(argv[2]) / sizeof(char);
    char **dict = read_dictionary(argv[2], &dlen);

    // Open the hash file for reading.
    FILE *f = fopen(argv[1], "r");

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    char *line[1000];
    while( fgets(line, 1000, f) != NULL )
    {

        for ( int i = 0; i < dlen; i++)
        {
            int a = tryguess(line[i], dict[i]);
            if (a)
            {
                printf("Matching dictionary entry: %s", dict[i]);
            }
        }
         
    }
    
    fclose(f);
}